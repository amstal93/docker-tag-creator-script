#!/bin/bash

docker build -t dockertagcreatorscript .

docker rm -f test-dtcs

docker run -it --name test-dtcs \
    -v /etc/localtime:/etc/localtime:ro \
    -v /etc/timezone:/etc/timezone:ro \
    dockertagcreatorscript
